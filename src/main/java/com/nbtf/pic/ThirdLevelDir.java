package com.nbtf.pic;

/**
 * Created by leo on 14/12/19.
 */
public class ThirdLevelDir {
    private String thirdLevelDirTopic = null;
    private String thirdLevelDirUrl = null;
    private String thirdLevelDirPicUrl = null;
    private String thirdLevelDirDesc = null;
    private long thirdLevelDirMnum = 0;

    public String getThirdLevelDirTopic() {
        return thirdLevelDirTopic;
    }

    public void setThirdLevelDirTopic(String thirdLevelDirTopic) {
        this.thirdLevelDirTopic = thirdLevelDirTopic;
    }

    public String getThirdLevelDirUrl() {
        return thirdLevelDirUrl;
    }

    public void setThirdLevelDirUrl(String thirdLevelDirUrl) {
        this.thirdLevelDirUrl = thirdLevelDirUrl;
    }

    public String getThirdLevelDirPicUrl() {
        return thirdLevelDirPicUrl;
    }

    public void setThirdLevelDirPicUrl(String thirdLevelDirPicUrl) {
        this.thirdLevelDirPicUrl = thirdLevelDirPicUrl;
    }

    public String getThirdLevelDirDesc() {
        return thirdLevelDirDesc;
    }

    public void setThirdLevelDirDesc(String thirdLevelDirDesc) {
        this.thirdLevelDirDesc = thirdLevelDirDesc;
    }

    public long getThirdLevelDirMnum() {
        return thirdLevelDirMnum;
    }

    public void setThirdLevelDirMnum(long thirdLevelDirMnum) {
        this.thirdLevelDirMnum = thirdLevelDirMnum;
    }

    @Override
    public String toString() {
        return "ThirdLevelDir{" +
                "thirdLevelDirTopic='" + thirdLevelDirTopic + '\'' +
                ", thirdLevelDirUrl='" + thirdLevelDirUrl + '\'' +
                ", thirdLevelDirPicUrl='" + thirdLevelDirPicUrl + '\'' +
                ", thirdLevelDirDesc='" + thirdLevelDirDesc + '\'' +
                ", thirdLevelDirMnum=" + thirdLevelDirMnum +
                '}'+'\n';
    }
}

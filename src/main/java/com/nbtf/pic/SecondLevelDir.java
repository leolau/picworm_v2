package com.nbtf.pic;
import com.nbtf.pic.ThirdLevelDir;

import java.util.ArrayList;

/**
 * Created by leo on 14/12/19.
 */
public class SecondLevelDir {
    private String topic;
    private String dirUrl;
    private ArrayList<ThirdLevelDir> thirdLevelDirs = new ArrayList<ThirdLevelDir>();
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getDirUrl() {
        return dirUrl;
    }

    public void setDirUrl(String dirUrl) {
        this.dirUrl = dirUrl;
    }

    public ArrayList<ThirdLevelDir> getThirdLevelDirs() {
        return thirdLevelDirs;
    }

    public void setThirdLevelDirs(ArrayList<ThirdLevelDir> thirdLevelDirs) {
        this.thirdLevelDirs = thirdLevelDirs;
    }

    @Override
    public String toString() {
        return "SecondLevelDir{" +
                "topic='" + topic + '\'' +
                ", dirUrl='" + dirUrl + '\'' +
                ", thirdLevelDirs=" + thirdLevelDirs +
                '}'+'\n';
    }
}
